import React from 'react';
import './Header.css';

function Header() {
  const today = new Date();
  const dateString = `${today.toLocaleDateString('fr-FR', { day: 'numeric', month: 'short', year: 'numeric' })}`;
  
  return (
    <div className="header">
      <h2>Todo</h2>
      <div className="date">Today<br/><span className="date-number">{dateString}</span></div>
    </div>
  );
}

export default Header;
