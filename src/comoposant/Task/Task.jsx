import React from 'react';

function Task({ task, toggleComplete, onDelete }) {
  return (
    <li className={`task${task.completed ? ' completed' : ''}`}>
      <input
        type="checkbox"
        checked={task.completed}
        onChange={() => toggleComplete(task.id)}
      />
      {task.text}
      <button onClick={() => onDelete(task.id)} className="delete-btn">X</button>
    </li>
  );
}

export default Task;