import React from 'react';
import Task from './../Task/Task';
import './TaskList.css';

function TaskList({ tasks, toggleComplete, onDelete }) {
  const sortedTasks = [...tasks].sort((a, b) => {
    if (a.completed === b.completed) {
      return 0; // Pas de changement si les deux tâches ont le même statut
    }
    return a.completed ? 1 : -1; // Les tâches non complétées viennent en premier
  });

  return (
    <ul className="task-list">
      {sortedTasks.map((task, index, array) => {
        // Vérifier si c'est la première tâche complétée
        const isFirstCompletedTask = task.completed && (index === 0 || !array[index - 1].completed);
        return (
          <React.Fragment key={task.id}>
            {/* Si c'est la première tâche complétée, ajouter une séparation */}
            {isFirstCompletedTask && <li className="task-separator">Tâches complétées</li>}
            <Task task={task} toggleComplete={toggleComplete} onDelete={onDelete} />
          </React.Fragment>
        );
      })}
    </ul>
  );
}

export default TaskList;
