import React, { useState, useEffect } from 'react';
import Header from './comoposant/Header/Header';
import AddTask from './comoposant/AddTask/AddTask';
import TaskList from './comoposant/TaskList/TaskList';
import './App.css';

function App() {
  const [tasks, setTasks] = useState([]);
  const [filter, setFilter] = useState('all'); // Ajout d'un état pour le filtre

  useEffect(() => {
    // Charger les tâches depuis le localStorage
    const storedTasks = JSON.parse(localStorage.getItem('tasks')) || [];
    setTasks(storedTasks);
  }, []);

  const saveToLocalStorage = (tasksToSave) => {
    localStorage.setItem('tasks', JSON.stringify(tasksToSave));
  };

  const addTask = (text) => {
    const newTask = { id: Date.now(), text, completed: false };
    const newTasks = [...tasks, newTask];
    setTasks(newTasks);
    saveToLocalStorage(newTasks);
  };

  const toggleComplete = (taskId) => {
    const updatedTasks = tasks.map((task) =>
      task.id === taskId ? { ...task, completed: !task.completed } : task
    );
    setTasks(updatedTasks);
    saveToLocalStorage(updatedTasks);
  };

  const deleteTask = (taskId) => {
    const remainingTasks = tasks.filter((task) => task.id !== taskId);
    setTasks(remainingTasks);
    saveToLocalStorage(remainingTasks);
  };

  // Fonction pour filtrer les tâches en fonction du filtre sélectionné
  const getFilteredTasks = () => {
    switch (filter) {
      case 'completed':
        return tasks.filter((task) => task.completed);
      case 'active':
        return tasks.filter((task) => !task.completed);
      default:
        return tasks;
    }
  };

  return (
    <div className="todo-container">
      <Header />
      <AddTask onAdd={addTask} />
      {/* Ajout des boutons de filtre */}
      <div className="filters">
         <button className={`filters-button ${filter === 'all' ? 'active' : ''}`} onClick={() => setFilter('all')}>Toutes</button>
        <button className={`filters-button ${filter === 'active' ? 'active' : ''}`} onClick={() => setFilter('active')}>En Court</button>
        <button className={`filters-button ${filter === 'completed' ? 'active' : ''}`} onClick={() => setFilter('completed')}>Complétées</button>

      </div>
      {/* Utilisation de getFilteredTasks au lieu de tasks directement */}
      <TaskList tasks={getFilteredTasks()} toggleComplete={toggleComplete} onDelete={deleteTask} />
    </div>
  );
}

export default App;
